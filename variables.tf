variable "disk_size" {
  description = "tamanho do disco"
  type        = number
  default     = 50
}

variable "public_key" {
  description = "chave publica"
  type        = string
  default     = ""
}
