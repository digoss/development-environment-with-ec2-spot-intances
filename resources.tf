# Create a VPC
resource "aws_vpc" "spot" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "spot" {
  vpc_id     = aws_vpc.spot.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Main"
  }
}

resource "aws_security_group" "spot-sg" {
  name_prefix = ""
  vpc_id      = aws_vpc.spot.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_internet_gateway" "spot-gateway" {
  vpc_id = aws_vpc.spot.id
}

resource "aws_route_table" "route-table-spot-env" {
  vpc_id = aws_vpc.spot.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.spot-gateway.id
  }
}

resource "aws_route_table_association" "subnet-association" {
  subnet_id      = aws_subnet.spot.id
  route_table_id = aws_route_table.route-table-spot-env.id
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"]
}

resource "aws_ebs_volume" "spot" {
  availability_zone = aws_subnet.spot.availability_zone
  size              = var.disk_size
}

data "aws_iam_policy" "ssm-manager-policy" {
  name = "AmazonSSMManagedInstanceCore"
}

data "aws_iam_policy_document" "policy_ebs_doc" {
  statement {
    actions = [
      "ec2:AttachVolume",
      "ec2:DetachVolume"
    ]
    resources = [
      "arn:aws:ec2:*:*:instance/*",
      "${aws_ebs_volume.spot.arn}"
    ]
  }
  statement {
    actions = [
      "ec2:DescribeVolumes"
    ]
    resources = ["${aws_ebs_volume.spot.arn}"]
  }
}

resource "aws_iam_role" "spot_ebs" {
  name_prefix = "ebs_spot_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy" "policy_ebs" {
  name_prefix = "policy_ebs"
  role        = aws_iam_role.spot_ebs.name
  policy      = data.aws_iam_policy_document.policy_ebs_doc.json
}

resource "aws_iam_role_policy_attachment" "attach_policy" {
  policy_arn = data.aws_iam_policy.ssm-manager-policy.arn
  role       = aws_iam_role.spot_ebs.name
}

resource "aws_iam_instance_profile" "ebs_spot" {
  name_prefix = "ebs_spot"
  role        = aws_iam_role.spot_ebs.name
}

resource "aws_key_pair" "docker" {
  key_name_prefix = "docker-key"
  public_key      = var.public_key
}

resource "aws_launch_template" "spot" {
  name_prefix = "spot"
  image_id    = data.aws_ami.ubuntu.image_id
  iam_instance_profile {
    arn = aws_iam_instance_profile.ebs_spot.arn
  }
  network_interfaces {
    subnet_id                   = aws_subnet.spot.id
    associate_public_ip_address = true
  }
  key_name  = aws_key_pair.docker.key_name
  user_data = base64encode(templatefile("${path.module}/ebs_attach.tftpl", { volume_id = aws_ebs_volume.spot.id, disk_size = var.disk_size }))
}

resource "aws_autoscaling_group" "spot" {
  availability_zones = [aws_subnet.spot.availability_zone]
  desired_capacity   = 1
  max_size           = 1
  min_size           = 1

  mixed_instances_policy {
    instances_distribution {
      on_demand_percentage_above_base_capacity = 0
    }
    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.spot.id
        version            = "$Latest"
      }

      override {
        instance_requirements {
          accelerator_count {
            max = 0
          }

          instance_generations = ["current"]

          local_storage_types = ["ssd"]

          memory_mib {
            min = 8000
            max = 16000
          }

          vcpu_count {
            min = 4
            max = 8
          }
        }
      }
    }
  }
}
