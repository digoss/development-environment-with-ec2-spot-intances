output "ami_name" {
  description = "AMI Name"
  value       = data.aws_ami.ubuntu.name
}

output "ami_id" {
  description = "AMI ID"
  value       = data.aws_ami.ubuntu.image_id
}

output "ami_owner" {
  description = "AMI Owner"
  value       = data.aws_ami.ubuntu.image_owner_alias
}